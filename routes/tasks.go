package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	//"strconv"
	//"strings"


	"github.com/gorilla/mux"
	"gitlab.com/agostinhodev/api-rest-go/database"
)

type Task struct {
	ID    		 int     `json:"id"`
	Description  string  `json:"description"`
}

var tasks []Task

func GetTasks(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		conn := database.SetConnection()

		selDB, err := conn.Query("SELECT * FROM task")

		if err != nil {
			fmt.Println("No tasks found", err)
		}

		for selDB.Next() {
			var task Task

			err = selDB.Scan(&task.ID, &task.Description)
			tasks = append(tasks, task)
		}

		encoder := json.NewEncoder(w)
		encoder.Encode(tasks)
	
	}

}

func GetTaskByID(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		conn := database.SetConnection()
		defer conn.Close()
		var task Task

		vars := mux.Vars(r)
		id := vars["taskID"]

		selDB := conn.QueryRow("SELECT id, description FROM task WHERE id=" + id)

		selDB.Scan(&task.ID, &task.Description)

		encoder := json.NewEncoder(w)
		encoder.Encode(task)

	}

}


func NewTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "POST" {
		w.WriteHeader(http.StatusCreated)
		conn := database.SetConnection()
		defer conn.Close()

		var register Task

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Fprint(w, "Bad Request")
		}

		json.Unmarshal(body, &register)
		
		action, err := conn.Prepare("INSERT INTO task (description) VALUES (?)")
		action.Exec(register.Description)

		encoder := json.NewEncoder(w)
		encoder.Encode(register)

	}

}

func UpdateTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "PUT" {
	
		w.WriteHeader(http.StatusCreated)
		conn := database.SetConnection()
		defer conn.Close()

		var register Task

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Fprint(w, "Bad Request")
		}

		vars := mux.Vars(r)
		id := vars["taskID"]

		json.Unmarshal(body, &register)
		
		action, err := conn.Prepare("UPDATE task SET description = ? WHERE id = " + id)
		action.Exec(register.Description)

		encoder := json.NewEncoder(w)
		encoder.Encode(register)

	}
}

func DeleteTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {
	
		w.WriteHeader(http.StatusOK)
		conn := database.SetConnection()
		defer conn.Close()

		var register Task

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Fprint(w, "Bad Request")
		}

		vars := mux.Vars(r)
		id := vars["taskID"]

		json.Unmarshal(body, &register)
		
		action, err := conn.Prepare("DELETE FROM task WHERE id = ?")
		action.Exec(id)

		fmt.Fprint(w, "Deletado com sucesso")

	}

	
}