package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/agostinhodev/api-rest-go/middlewares"
	"gitlab.com/agostinhodev/api-rest-go/routes"
)

func welcomeRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "welcome")
}

func setRoutes(router *mux.Router) {
	
	router.HandleFunc("/", welcomeRoute)
	router.HandleFunc("/tasks", routes.GetTasks)
	router.HandleFunc("/tasks/{taskID}", routes.GetTaskByID)
	router.HandleFunc("/newtask", routes.NewTask)
	router.HandleFunc("/updatetask/{taskID}", routes.UpdateTask)
	router.HandleFunc("/deletetask/{taskID}", routes.DeleteTask)

}

func main() {
	var router *mux.Router

	router = mux.NewRouter()

	router.Use(middlewares.JsonMiddleware)

	setRoutes(router)

	err := http.ListenAndServe(":3956", router)
	if err != nil {
		fmt.Println("Error", err)
	}
}
